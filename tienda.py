'''Maneja un diccionario con los artículos de una tienda y
sus precios, y permite luego comprar alguno de sus elementos,
y ver el precio total de la compra.
'''
import sys

articulos = {}

def anadir(articulo, precio):#Añade un artículo y un precio al diccionario artículos
    articulos[articulo] = precio

def mostrar():#Muestra en pantalla todos los artículos y sus precios
    for articulo, precio in articulos.items():
        print(articulo + ":", float(precio), "euros")

def pedir_articulo() -> str:#pide al usuario el articulo a comprar
    articulo = str(input("Artículo: "))
    if articulo in articulos:
        return articulo
    else:
        while articulo not in articulos:
            articulo = str(input("Artículo: "))
    return articulo

def pedir_cantidad() -> float:#pide al usuario la cantidad a comprar
    try:
        cantidad = float(input("Cantidad: "))
        return cantidad
    except ValueError:
        return pedir_articulo()

def main():
    sys.argv.pop(0)
    for i in range(0, len(sys.argv), 2):
        try:
            articulo = sys.argv[i]
            precio = float(sys.argv[i + 1])
            anadir(articulo, precio)
            introducido = True
        except IndexError:
            print("error en argumentos, hay al menos un articulo in precio: ", articulo)

    try:
        if introducido:
            print("Lista de artículos en la tienda:")
            mostrar()
            print()
            compra = pedir_articulo()
            cantidad = float(pedir_cantidad())
            total = float(articulos[compra]) * cantidad
            print()
            print("Compra total:", cantidad, "de", compra + ", a pagar", total)
    except UnboundLocalError:
        print("error en argumentos: no se han especificado articulos")
        sys.exit()

if __name__ == '__main__':
    main()
